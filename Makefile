.POSIX:
.SUFFIXES:

AS=sdasz80
CC=sdcc
LD=sdldz80
BURNER=miniprohex
BURNERFLAGS=-f ihex -p $(ROMTYPE)
ASFLAGS=-o
CFLAGS=-mz80 -Iinclude
LDFLAGS=-m -i
ROMTYPE=AT28C256

all: pz80os.ihx
bin: pz80os.bin

pz80os.bin: pz80os.ihx
kernel_start.rel: kernel_start.asm
kernel.rel: kernel.c include/devmap.h include/sio.h
sio.rel: sio.c include/devmap.h include/sio.h

pz80os.ihx: kernel_start.rel sio.rel kernel.rel
	$(LD) $(LDFLAGS) pz80os kernel_start.rel sio.rel kernel.rel

rom: pz80os.ihx
	-$(BURNER) $(BURNERFLAGS) -w $<
clean:
	rm -f *.rel *.lst *.sym *.ihx *.bin kernel.asm sio.asm

.SUFFIXES: .ihx .bin .asm .c .rel
.ihx.bin:
	makebin $< $@
.asm.rel:
	$(AS) $(ASFLAGS) $<
.c.rel:
	$(CC) $(CFLAGS) -c $<

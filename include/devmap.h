#ifndef __DEVMAP_H
#define __DEVMAP_H

__sfr __at 0x00 Sio_A_D;
__sfr __at 0x01 Sio_B_D;
__sfr __at 0x02 Sio_A_C;
__sfr __at 0x03 Sio_B_C;

#endif /* __DEVMAP_H */

#ifndef __SIO_H
#define __SIO_H

#define SIO_NULL		0b00000000
#define SIO_SELECT_WR1		0b00000001
#define SIO_SELECT_WR2		0b00000010
#define SIO_SELECT_WR3		0b00000011
#define SIO_SELECT_WR4		0b00000100
#define SIO_SELECT_WR5		0b00000101
#define SIO_SELECT_WR6		0b00000110
#define SIO_SELECT_WR7		0b00000111
#define SIO_SEND_ABORT		0b00001000
#define SIO_RESET_EXT_INT	0b00010000
#define SIO_CHANNEL_RESET	0b00011000
#define SIO_EN_INT_NEXT_RX_CHAR	0b00100000
#define SIO_RESET_TX_INT_PEND	0b00101000
#define SIO_ERROR_RESET		0b00110000
#define SIO_RETURN_FROM_INT	0b00111000
#define SIO_WR1_ENABLE_EXT_INT	(1 << 0)
#define SIO_WR1_ENABLE_TX_INT	(1 << 1)
#define SIO_WR1_STAT_AFF_VECT	(1 << 2)
#define SIO_WR1_RX_IM0		(1 << 3)
#define SIO_WR1_RX_IM1		(1 << 4)
#define SIO_WR1_WAIT_RDY_RX_TX	(1 << 5)
#define SIO_WR1_WAIT_RDY_FUNC	(1 << 6)
#define SIO_WR1_ENABLE_WAIT_RDY	(1 << 7)
#define SIO_WR3_RECEIVER_ENABLE	(1 << 0)
#define SIO_WR3_SNC_CH_LD_INHBT	(1 << 1)
#define SIO_WR3_ADDR_SRCH_MODE	(1 << 2)
#define SIO_WR3_RX_CRC_ENABLE	(1 << 3)
#define SIO_WR3_ENTER_HUNT_PHSE	(1 << 4)
#define SIO_WR3_AUTO_ENABLES	(1 << 5)
#define SIO_WR3_RX_BITS_CHAR_0	(1 << 6)
#define SIO_WR3_RX_BITS_CHAR_1	(1 << 7)
#define SIO_WR4_PARITY		(1 << 0)
#define SIO_WR4_PARITY_EVEN_ODD	(1 << 1)
#define SIO_WR4_STOP_BITS_0	(1 << 2)
#define SIO_WR4_STOP_BITS_1	(1 << 3)
#define SIO_WR4_SYNC_MODES_0	(1 << 4)
#define SIO_WR4_SYNC_MODES_1	(1 << 5)
#define SIO_WR4_CLOCK_RATE_0	(1 << 6)
#define SIO_WR4_CLOCK_RATE_1	(1 << 7)
#define SIO_WR5_TX_CRC_ENABLE	(1 << 0)
#define SIO_WR5_RTS		(1 << 1)
#define SIO_WR5_CRC16_SDLC	(1 << 2)
#define SIO_WR5_TX_ENABLE	(1 << 3)
#define SIO_WR5_SEND_BREAK	(1 << 4)
#define SIO_WR5_TX_BITS_CHAR_0	(1 << 5)
#define SIO_WR5_TX_BITS_CHAR_1	(1 << 6)
#define SIO_WR5_DTR		(1 << 7)

void reset_channel(unsigned int);
void initialize_channel_a(void);

#endif /* __SIO_H */

#include <devmap.h>
#include <sio.h>

void sio_reset_a(void) {
	Sio_A_C = SIO_ERROR_RESET;
	Sio_A_C = SIO_CHANNEL_RESET;
}

void initialize_channel_a(void) {
	sio_reset_a();

	Sio_A_C = SIO_SELECT_WR4;

	/* 16x Clock, No Parity, 1 Stop Bit */
	Sio_A_C = SIO_WR4_STOP_BITS_0 | SIO_WR4_CLOCK_RATE_0;

	Sio_A_C = SIO_SELECT_WR5;

	/* DTR enabled, 8 bit, TX enabled */
	Sio_A_C = (unsigned)(SIO_WR5_DTR | SIO_WR5_TX_BITS_CHAR_0 | SIO_WR5_TX_BITS_CHAR_1 | SIO_WR5_TX_ENABLE);

	Sio_B_C = SIO_SELECT_WR1;

	/* Status change affects interrupt vectors */
	Sio_B_C = SIO_WR1_STAT_AFF_VECT;

	/* interrupt vector at 0x40 */
	Sio_B_C = SIO_SELECT_WR2;
	Sio_B_C = 0x40;
}
